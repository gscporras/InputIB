import { Observable } from 'tns-core-modules/data/observable';
import { Inputibk } from 'nativescript-inputibk';

export class HelloWorldModel extends Observable {
  public message: string;
  private inputibk: Inputibk;

  constructor() {
    super();

    this.inputibk = new Inputibk();
    this.message = this.inputibk.message;
  }
}
