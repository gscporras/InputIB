var Inputibk = require("nativescript-inputibk").Inputibk;
var inputibk = new Inputibk();

describe("greet function", function() {
    it("exists", function() {
        expect(inputibk.greet).toBeDefined();
    });

    it("returns a string", function() {
        expect(inputibk.greet()).toEqual("Hello, NS");
    });
});